import argparse
import copy
import networkx as nx
import yaml

from collections import defaultdict

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET


def strip_namespace(tag):
    if '}' in tag:
        tag = tag.split('}', 1)[1]
    return tag


class Task:
    def __init__(self, id, name, runtime, input_files=None, output_files=None):
        self.id = id
        self.name = name
        self.runtime = runtime
        if input_files is None:
            self.input_files = {}
        else:
            self.input_files = input_files
        if output_files is None:
            self.output_files = {}
        else:
            self.output_files = output_files
        self.parents = set()


class File:
    def __init__(self, name, size, transfer):
        self.name = name
        self.size = size
        self.transfer = transfer
        self.idx = None
        self.sources = list()


def main():
    parser = argparse.ArgumentParser(description="DAX to YAML converter")
    parser.add_argument("input_file", type=str, help="path to DAX file")
    parser.add_argument("output_file", type=str, help="path to YAML file")
    parser.add_argument("-s", "--speed", type=int, default=4200000000,
                        help="host speed in flop/s used to convert task runtime to flop, "
                             "default value is 4200000000 (see simdag/sd_daxloader.cpp)")
    parser.add_argument("--no-boundary", action="store_true", default=False, help="omit root and end tasks")
    parser.add_argument("-d", "--data-scaling", type=float, default=1.0, help="Data size scaling factor")

    args = parser.parse_args()

    tree = ET.parse(args.input_file)
    tasks = {}
    file_producers = defaultdict(list)
    file_consumers = defaultdict(list)

    root = Task('root', 'root', 0.)
    end = Task('end', 'end', 0.)
    tasks[root.id] = root
    tasks[end.id] = end

    for el in tree.getroot():
        tag = strip_namespace(el.tag)
        if tag == 'job':
            task_id = el.attrib['id']
            task_name = el.attrib['name']
            runtime = float(el.attrib['runtime'])
            input_files = {}
            output_files = {}
            for sub_el in el:
                sub_tag = strip_namespace(sub_el.tag)
                if sub_tag == 'uses':
                    file_name = sub_el.attrib['file']
                    file_size = float(sub_el.attrib['size'])
                    link = sub_el.attrib['link']
                    transfer = sub_el.attrib['transfer']

                    file = File(file_name, file_size, transfer)

                    if link == 'input':
                        input_files[file_name] = file
                        file.idx = len(input_files)
                        file_consumers[file_name].append(task_id)
                    elif link == 'output':
                        output_files[file_name] = file
                        file.idx = len(output_files)
                        # assert file_name not in file_producers, \
                        #     "Multiple producers for file %s: %s, %s" % (file_name, file_producers[file_name], task_id)
                        file_producers[file_name].append(task_id)
            task = Task(task_id, task_name, runtime, input_files, output_files)
            assert task_id not in tasks, "Duplicate task id"
            tasks[task_id] = task
            # dag.add_node(task, weight=task.runtime)
        elif tag == 'child':
            task = tasks[el.attrib['ref']]
            for sub_el in el:
                sub_tag = strip_namespace(sub_el.tag)
                if sub_tag == 'parent':
                    parent = tasks[sub_el.attrib['ref']]
                    task.parents.add(parent.id)

    print(file_producers['diff.txt'])

    dag = nx.DiGraph()

    for task in tasks.values():
        weight = task.runtime * args.speed
        dag.add_node(task, weight=weight)

    for task in tasks.values():
        for file in task.input_files.values():
            if file.name in file_producers:
                # assert file.transfer == 'false'
                producers = file_producers[file.name]
            else:
                # input files not produced in DAG are outputs of the root task
                # assert file.transfer == 'true', 'Wrong file transfer for file %s in task %s?' % (file.name, task.id)
                if file.name not in root.output_files:
                    file_ = copy.copy(file)
                    root.output_files[file.name] = file_
                    file_.idx = len(root.output_files)
                producers = [root.id]
                task.parents.add(root.id)
            for producer_id in producers:
                if producer_id in task.parents:
                    parent = tasks[producer_id]
                    parent_file = parent.output_files[file.name]
                    weight = file.size
                    try:
                        assert parent_file.size == file.size, \
                            "File %s sizes differ: producer %s %f, consumer %s %f)" % \
                            (file.name, parent.id, parent_file.size, task.id, file.size)
                    except AssertionError as e:
                        # print(e)
                        weight = parent_file.size

                    # pysimgrid does not support multigraphs, i.e. multiple data transfers between same tasks
                    # (see simulation.get_task_graph())
                    # therefore multiple data transfers are converted to a single edge with total data size
                    if not dag.has_edge(parent, task):
                        dag.add_edge(parent, task, weight=weight)
                    else:
                        # print("!!! Duplicate edge: %s -> %s" % (parent.id, task.id))
                        dag[parent][task]['weight'] += weight

                    file.sources.append((parent.id, parent_file.idx))

        # output files not consumed in DAG are inputs to the end task
        for file in task.output_files.values():
            if file.name not in file_consumers or len(file_consumers[file.name]) == 0:
                assert file.transfer == 'true'
                if not dag.has_edge(task, tasks['end']):
                    dag.add_edge(task, tasks['end'], weight=file.size)
                    tasks['end'].parents.add(task.id)
                else:
                    dag[task][tasks['end']]['weight'] += file.size

                file_ = copy.copy(file)
                end.input_files[file.name] = file_
                file_.idx = len(end.input_files)
                file_.sources.append((task.id, file.idx))
            # else:
            #     assert file.transfer == 'false', 'Wrong file transfer for file %s in task %s?' % (file.name, task.id)

        # check that edges for all parent tasks exist
        for parent_id in task.parents:
            parent = tasks[parent_id]
            assert dag.has_edge(parent, task), "Non-data dependency: %s -> %s" % (parent_id, task.id)

        # check that no edges for non-parent tasks exist
        for src, dst in dag.edges():
            assert src.id in dst.parents, "Wrong edge, %s is not parent of %s" % (src.id, dst.id)



    wf_tasks = []
    comp_sizes = []
    data_sizes = []
    for task in tasks.values():
        inputs = {}
        inputs_size = 0
        idx = 1
        for file in task.input_files.values():
            for (source, source_idx) in file.sources:
                inputs['input%d' % idx] = '${%s.output%d}' % (source, source_idx)
                inputs_size += (file.size / 1E6) * args.data_scaling
                idx += 1
        from random import randint
        comp_size = task.runtime * args.speed / 1E9
        inputs['comp_size'] = comp_size
        comp_sizes.append(comp_size)
        output_sizes = [None] * len(task.output_files)
        for file in task.output_files.values():
            output_size = (file.size / 1E6) * args.data_scaling
            output_sizes[file.idx - 1] = output_size
            data_sizes.append(output_size)
        inputs['output_sizes'] = output_sizes
        if task.id == 'root':
            print("INPUT DATA: %f" % sum(output_sizes))
        if task.id == 'end':
            print("OUTPUT DATA: %f" % inputs_size)
        wf_tasks.append({
            'name': task.id,
            'app': 'admin/synthetic',
            'inputs': inputs
        })

    wf = {
        "name": args.input_file.split("/")[-1],
        "inputs": [],
        "outputs": [],
        "jobs": wf_tasks
    }

    yaml.add_representer(dict, lambda self, data: yaml.representer.SafeRepresenter.represent_dict(self, data.items()))
    with open(args.output_file, 'w') as out:
        out.write(yaml.dump(wf))

    max_comp = max(comp_sizes)
    min_comp = min(comp_sizes)
    total_comp = sum(comp_sizes)
    avg_comp = total_comp/len(comp_sizes)
    print("COMP SIZES: %f %f %f %f" % (min_comp, avg_comp, max_comp, total_comp))
    max_data = max(data_sizes)
    min_data = min(data_sizes)
    total_data = sum(data_sizes)
    avg_data = total_data/len(data_sizes)
    print("DATA SIZES: %f %f %f %f" % (min_data, avg_data, max_data, total_data))

if __name__ == "__main__":
    main()
