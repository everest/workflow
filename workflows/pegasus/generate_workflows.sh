#!/bin/sh


# MONTAGE 100 -----------------------------------------------------------------------------------------------

# NO DATA
# COMP SIZES: 0.000000 44.443412 58.170000 4533.228000
python3 dax_to_yaml.py -d 0 dax/Montage_100.xml yaml/montage-100-0data.yaml

# INPUT DATA: 67.560634
# OUTPUT DATA: 0.420195
# COMP SIZES: 0.000000 44.443412 58.170000 4533.228000
# DATA SIZES: 0.000250 2.298402 69.432167 494.156379
python3 dax_to_yaml.py dax/Montage_100.xml yaml/montage-100.yaml


# INSPIRAL 100 -----------------------------------------------------------------------------------------------

# NO DATA
# COMP SIZES: 0.000000 103.058627 335.225000 10511.980000
python3 dax_to_yaml.py -s 500000000 -d 0 dax/Inspiral_100.xml yaml/inspiral-100-0data.yaml

# INPUT DATA: 380.196623
# OUTPUT DATA: 0.052859
# COMP SIZES: 0.000000 103.058627 335.225000 10511.980000
# DATA SIZES: 0.003782 2.643310 7.816492 399.139827
python3 dax_to_yaml.py -d 0.5 -s 500000000 dax/Inspiral_100.xml yaml/inspiral-100.yaml


# EPIGENOMICS 100 -----------------------------------------------------------------------------------------------

# NO DATA
# COMP SIZES: 0.000000 39.549039 235.714900 4034.002000
python3 dax_to_yaml.py -s 10000000 -d 0 dax/Epigenomics_100.xml yaml/epigenomics-100-0data.yaml

# INPUT DATA: 1083.686327
# OUTPUT DATA: 12.029816
# COMP SIZES: 0.000000 39.549039 235.714900 4034.002000
# DATA SIZES: 0.011372 7.552821 530.094765 1148.028845
python3 dax_to_yaml.py -d 0.1 -s 10000000 dax/Epigenomics_100.xml yaml/epigenomics-100.yaml


# CYBERSHAKE 100 -----------------------------------------------------------------------------------------------

# NO DATA
# COMP SIZES: 0.000000 43.855098 329.480000 4473.220000
python3 dax_to_yaml.py -s 2000000000 -d 0 dax/CyberShake_100.xml yaml/cybershake-100-0data.yaml

# INPUT DATA: 4086.025633
# OUTPUT DATA: 0.007451
# COMP SIZES: 0.000000 43.855098 329.480000 4473.220000
# DATA SIZES: 0.000011 26.905823 1025.847635 4143.496667
python3 dax_to_yaml.py -d 0.05 -s 2000000000 dax/CyberShake_100.xml yaml/cybershake-100.yaml
