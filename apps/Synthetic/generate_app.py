import json

app = {
  "alias" : "synthetic",
  "name" : "Synthetic",
  "annotation" : "",
  "keywords" : [],
  "description" : "",
  "inputs": {
    "comp_size" : {
      "title" : "Computational size",
      "description" : "Computational size in Gflops",
      "type" : "number",
      "minimum" : 0,
      "required" : True
    },
    "output_sizes" : {
      "title" : "Output sizes",
      "description" : "Sizes of produced output files in Mbytes",
      "type" : "array",
      "items" : {
        "type" : "integer",
        "minimum" : 0
      },
      "required" : True
    }
  },
  "outputs": {},
  "type" : "application",
  "config" : {
    "command" : "./synthetic_task.py ${comp_size} ${output_sizes}",
    "inputMappings" : [],
    "outputMappings" : [],
    "resourceRequirements" : {
      "nodes" : {
        "defaultValue" : 1,
        "allowOverride" : False
      },
      "coresPerNode" : {
        "defaultValue" : 1,
        "allowOverride" : False
      },
      "memoryPerCore" : {
        "defaultValue" : 1,
        "allowOverride" : False
      },
      "timeLimit" : {
        "defaultValue" : 1,
        "allowOverride" : False
      }
    }
  },
  "retryFailedTasks" : False,
  "files" : [ {
    "uri" : "file:///ApplicationFiles/synthetic_task.py",
    "public" : False,
    "stageIn" : True
  } ],
  "version" : "0.1.0"
}


for i in range(1, 126):
    app["inputs"]["input%d" % i] = {
      "title" : "Input %d" % i,
      "description" : "",
      "type" : "string",
      "format" : "uri",
      "viewType" : "input",
      "required" : False
    }
    app["config"]["inputMappings"].append({
      "param" : "input%d" % i,
      "file" : "input%d" % i,
      "pattern" : ""
    })

for i in range(1, 101):
    app["outputs"]["output%d" % i] = {
      "title" : "Output %d" % i,
      "description" : "",
      "type" : "string",
      "format" : "uri",
      "required" : False
    }
    app["config"]["outputMappings"].append({
      "param" : "output%d" % i,
      "file" : "output%d" % i,
      "pattern" : ""
    })    


with open("application.json", "w") as f:
    json.dump(app, f, indent=2)
