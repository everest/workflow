import argparse
import yaml


def read_profile(file):
    data = ''
    with open(file, 'r') as f:
        for line in f:
            if '!!int' in line:
                line = line.replace(".0'", "'")
            data += line
    raw_profile = yaml.load(data)
    profile = {}
    for job in raw_profile['jobs']:
        profile[job['name']] = job
    return profile


def profile_stats(profile):
    overheads = []
    for job in profile.values():
        overhead = job['totalTime'] - job['runTime']
        overheads.append(overhead)
    print("MIN OVERHEAD:", min(overheads))
    print("MAX OVERHEAD:", max(overheads))
    print("MEAN OVERHEAD:", sum(overheads)/len(overheads))
    print("TOTAL OVERHEAD:", sum(overheads))


def main():
    parser = argparse.ArgumentParser(description="Compare profiles")
    parser.add_argument("profile1_file", type=str, help="path to profile 1")
    parser.add_argument("profile2_file", type=str, help="path to profile 2")

    args = parser.parse_args()

    profile1 = read_profile(args.profile1_file)
    profile_stats(profile1)
    profile2 = read_profile(args.profile2_file)
    profile_stats(profile2)

    for (id, job) in profile1.items():
        other = profile2[id]
        totalTimeDiff = job['totalTime'] - other['totalTime']
        runtimeDiff = job['runTime'] - other['runTime']
        stageInDiff = job['stageInTime'] - other['stageInTime']
        stageOutDiff = job['stageOutTime'] - other['stageOutTime']
        print(id, "\t", totalTimeDiff, "\t", runtimeDiff, "\t", stageInDiff, "\t", stageOutDiff)


if __name__ == "__main__":
    main()
